package leetcode_0026;

public class Main {
  public int removeDuplicates(int[] nums) {
    if (nums.length < 1) {
      return nums.length;
    }

    int flag = 0;
    for (int i = 1; i < nums.length; i++) {
      if (nums[flag] != nums[i]) {
        nums[flag + 1] = nums[i];
        flag++;
      }
    }
    return flag+1;
  }
}
