package leetcode_0083;

public class Main {

  /**
   * https://leetcodechina.com/problems/remove-duplicates-from-sorted-list/description/
   * <p>
   * 给定一个排序链表，删除所有重复的元素使得每个元素只留下一个。
   *
   * @param head 头
   * @return 结果的头
   * <p>
   * <p>
   * 无修改一遍过。就是这么屌。
   */
  public ListNode deleteDuplicates(ListNode head) {
    if (head == null) {
      return null;
    }
    ListNode first = head;
    while (head.next != null) {
      if (head.val == head.next.val) {
        head.next = head.next.next;
      } else {
        head = head.next;
      }
    }
    return first;
  }


}
