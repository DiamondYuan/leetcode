package leetcode_0202;

import java.util.HashSet;
import java.util.Set;

public class Main {


  /**
   * 第202题 https://leetcodechina.com/problems/happy-number/description/
   * <p>
   * 写一个算法来判断一个数是不是“快乐数”。
   * 一个数是不是快乐是这么定义的：对于一个正整数，每一次将该数替换为它每个位置上的数字的平方和，然后重复这个过程直到这个数变为 1，或是无限循环但始终变不到 1。如果可以变为 1，那么这个数就是快乐数。
   *
   * @param n 数字
   * @return 结果
   */
  public boolean isHappy(int n) {
    Set<Integer> temp = new HashSet<>();
    return isHappy(n, temp);
  }

  public boolean isHappy(int n, Set<Integer> temp) {
    if (!temp.add(n)) {
      return false;
    }
    if (n == 1) {
      return true;
    }
    int result = 0;
    while (n > 0) {
      result += (n % 10) * (n % 10);
      n = n / 10;
    }
    if (result == 1) {
      return true;
    }
    return isHappy(result, temp);
  }

  public static void main(String[] args) {
    System.out.println(new Main().isHappy(19));
  }


}
