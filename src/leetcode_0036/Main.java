package leetcode_0036;

public class Main {

  /**
   * 第36题 https://leetcodechina.com/problems/valid-sudoku/description/
   * 判断一个数独是否有效，根据：Sudoku Puzzles - The Rules。
   * 数独部分填了数字，空的部分用 '.' 表示。
   * 一个有效的数独（填了一部分的）不一定是可解的，只要已经填的数字是有效的即可。
   *
   * @param board 数独字符串
   * @return 是否有效
   */
  public boolean isValidSudoku(char[][] board) {

    if (board.length != 9) {
      return false;
    }
    for (int i = 0; i < 9; i++) {
      if (board[i].length != 9) {
        return false;
      }
      if (!isValid(board[i])) {
        return false;
      }
    }
    for (int i = 0; i < 9; i++) {
      char[] temp = new char[9];
      for (int j = 0; j < 9; j++) {
        temp[j] = board[j][i];
      }
      if (!isValid(temp)) {
        return false;
      }
    }
    for (int i = 0; i < 9; i++) {
      char[] temp = new char[9];
      for (int j = 0; j < 9; j++) {
        temp[j] = board[j / 3 + (i / 3) * 3][j % 3 + (i % 3) * 3];
      }
      if (!isValid(temp)) {
        return false;
      }
    }
    return true;
  }

  public boolean isValid(char[] line) {
    int[] temp = new int[9];
    for (char c : line) {
      if (c != '.') {
        if (temp[c - '1'] != 0) {
          return false;
        } else {
          temp[c - '1'] = 1;
        }
      }
    }
    return true;
  }
}
