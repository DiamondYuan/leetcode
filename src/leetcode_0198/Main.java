package leetcode_0198;

public class Main {


  /**
   * Leetcode 198 https://leetcodechina.com/problems/house-robber/description/
   * <p>
   * 你是一个专业的强盗，计划抢劫沿街的房屋。每间房都藏有一定的现金，阻止你抢劫他们的唯一的制约因素就是相邻的房屋有保安系统连接，如果两间相邻的房屋在同一晚上被闯入，它会自动联系警方。
   * 给定一个代表每个房屋的金额的非负整数列表，确定你可以在没有提醒警方的情况下抢劫的最高金额。
   *
   * @param nums 给定一个代表每个房屋的金额的非负整数列表
   * @return 抢劫的最高金额
   */
  public int rob(int[] nums) {
    if (nums == null || nums.length < 1) {
      return 0;
    }
    int[] pick = new int[nums.length];
    int[] unPick = new int[nums.length];
    pick[0] = nums[0];
    unPick[0] = 0;
    for (int i = 1; i < nums.length; i++) {
      pick[i] = unPick[i - 1] + nums[i] > pick[i - 1] ? unPick[i - 1] + nums[i] : pick[i - 1];
      unPick[i] = pick[i - 1] > unPick[i - 1] ? pick[i - 1] : unPick[i - 1];
    }
    return pick[nums.length - 1] > unPick[nums.length - 1] ? pick[nums.length - 1] : unPick[nums.length - 1];
  }


}
