package leetcode_0215;

public class Main {


  public static int findKthLargest(int[] nums, int k) {
    int[] heap = new int[k];
    //首先取k个元素
    System.arraycopy(nums, 0, heap, 0, k);
    //从倒数k个节点开始 调整数组成为最小堆
    for (int i = k / 2 - 1; i >= 0; i--) {
      adjest(heap, i);
    }
    //如果元素小于最小堆 跳过。
    //如果元素大于最小堆 把元素放在堆顶 然后调整堆
    for (int i = k; i < nums.length; i++) {
      if (heap[0] < nums[i]) {
        heap[0] = nums[i];
        adjest(heap, 0);
      }
    }
    //返回堆顶堆元素
    return heap[0];
  }


  /**
   * 调整最小堆
   * @param heap 堆
   * @param i 从哪个 index 开始
   */
  private static void adjest(int[] heap, int i) {
    int temp = heap[i];
    int length = heap.length;
    for (int k = i * 2 + 1; k < length; k = 2 * k + 1) {
      if (k + 1 < length && heap[k + 1] < heap[k]) {
        k++;
      }
      if (temp <= heap[k]) {
        break;
      } else {
        heap[i] = heap[k];
        i = k;
      }
    }
    heap[i] = temp;
  }


}
