package leetcode_0110;

public class Main {


  private int max = Integer.MIN_VALUE;
  private int min = Integer.MAX_VALUE;


  public boolean isBalanced(TreeNode root) {
    if (root == null || root.right == null && root.left == null) {
      return true;
    }
    return deep(root, 0);
  }


  public boolean deep(TreeNode root, int length) {
    if (root == null) {
      max = Math.max(max, length + 1);
      min = Math.min(min, length + 1);
      System.out.println(max + "   "+ min);
      return Math.abs(max - min) < 2;
    }
    return deep(root.left, length + 1) && deep(root.right, length + 1);
  }


  public static void main(String[] args) {
    TreeNode treeNode = new TreeNode(3);
    treeNode.left = new TreeNode(3);
    treeNode.left.right = new TreeNode(3);
    System.out.println(new Main().isBalanced(treeNode));
  }


}
