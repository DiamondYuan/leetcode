package leetcode_0103;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Main {


  /**
   * 103. 二叉树的锯齿形层次遍历 https://leetcodechina.com/problems/binary-tree-zigzag-level-order-traversal/description/
   *
   * 给定一个二叉树，返回其节点值的锯齿形层次遍历。（即先从左往右，再从右往左进行下一层遍历，以此类推，层与层之间交替进行）。
   *
   * @param root 二叉树的跟节点
   * @return 结果
   */
  public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
    Stack<TreeNode> stack = new Stack<>();
    List<List<Integer>> result = new ArrayList<>();
    if (root == null) {
      return result;
    }
    boolean firstRight = false;
    result.add(new ArrayList<Integer>() {{
      add(root.val);
    }});
    pushToStack(stack, root, firstRight);
    firstRight = !firstRight;
    while (!stack.isEmpty()) {
      Stack<TreeNode> stack2 = new Stack<>();
      List<Integer> integers = new ArrayList<>();
      while (!stack.isEmpty() && stack.peek() != null) {
        TreeNode treeNode = stack.pop();
        integers.add(treeNode.val);
        pushToStack(stack2, treeNode, firstRight);
      }
      firstRight = !firstRight;
      result.add(integers);
      stack = stack2;

    }
    return result;
  }


  private void pushToStack(Stack<TreeNode> stack, TreeNode root, Boolean firstRight) {
    if (firstRight) {
      push(stack, root.right);
      push(stack, root.left);
    } else {
      push(stack, root.left);
      push(stack, root.right);
    }
  }

  private void push(Stack<TreeNode> stack, TreeNode treeNode) {
    if (treeNode != null) {
      stack.push(treeNode);
    }
  }


}
