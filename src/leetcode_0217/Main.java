package leetcode_0217;

import java.util.HashSet;
import java.util.Set;

public class Main {


  /**
   * 第217题目 https://leetcodechina.com/problems/contains-duplicate/description/
   *
   * 给定一个整数数组，判断是否存在重复元素。
   * 如果任何值在数组中出现至少两次，函数应该返回 true。如果每个元素都不相同，则返回 false。
   *
   * @param nums 数组
   * @return 存在返回 true
   */
  public boolean containsDuplicate(int[] nums) {
    if(nums.length < 2){
      return false;
    }
    Set<Integer> in = new HashSet<>();
    for(int i =0;i<nums.length;i++){
      if(!in.add(nums[i])){
        return true;
      }
    }
    return false;
  }








}
