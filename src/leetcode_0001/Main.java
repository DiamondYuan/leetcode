package leetcode_0001;

public class Main {


  /**
   * 第一题 https://leetcodechina.com/problems/two-sum/description/
   * <p>
   * 给定一个整数字符串，找出和为特定数字的两个数。
   * 两数和（twoSum）这个函数应该返回和为目标值的两个数字的下标。
   * 可以假设每个输入都会只有一种答案，并且同样的元素不能被重用。
   *
   * @param nums   输入的字符串
   * @param target 需求的和
   * @return 返回和为目标值的两个数字的下标
   */
  public int[] twoSum(int[] nums, int target) {
    int[] result = new int[2];
    for (int i = 0; i < nums.length - 1; i++) {
      for (int j = i + 1; j < nums.length; j++) {
        if (nums[i] + nums[j] == target) {
          result[0] = i;
          result[1] = j;
          return result;
        }
      }
    }
    return result;
  }









}
