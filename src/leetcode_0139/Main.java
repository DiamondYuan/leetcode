package leetcode_0139;

import java.util.List;

public class Main {


  public boolean wordBreak(String s, List<String> wordDict) {

    if (s.length() == 0) {
      return true;
    }
    int len = s.length();
    boolean[] result = new boolean[len + 1];
    result[0] = true;
    for (int i = 1; i <= len; i++) {
      for (int j = 0; j < i; j++) {
        if (wordDict.contains(s.substring(j, i)) && result[j]) {
          result[i] = true;
          break;
        }

      }

    }
    return result[len];
  }


}
