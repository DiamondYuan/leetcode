package leetcode_0303;

public class NumArray {


  private int[] tempResult;
  private int[] tempNums;

  public NumArray(int[] nums) {
    if (nums == null || nums.length == 0) {
      return;
    }
    tempNums = nums;
    tempResult = new int[nums.length];
    tempResult[0] = nums[0];
    if (nums.length == 1) {
      return;
    }
    for (int i = 1; i < nums.length; i++) {
      tempResult[i] = tempResult[i - 1] + nums[i];
    }
  }

  public int sumRange(int i, int j) {
    return tempResult[j] - tempResult[i] + tempNums[i];
  }


}
