package test.heap;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by chengxiao on 2016/12/17.
 * 堆排序demo
 */
public class HeapSort {

  public static void main(String[] args) {
    Random random = new Random(System.currentTimeMillis());
    int[] arr = new int[10];
    for (int i = 0; i < arr.length; i++) {
      arr[i] = random.nextInt(1000);
    }
    System.out.println(Arrays.toString(arr));
    sort(arr);
    System.out.println(Arrays.toString(arr));


  }


  private static void sort(int[] arr) {
    for (int i = arr.length / 2 - 1; i >= 0; i--) {
      flow(arr, i, arr.length);
    }
    for (int j = arr.length - 1; j > 0; j--) {
      swap(arr, 0, j);
      flow(arr, 0, j);
    }

  }


  private static void flow(int[] arr, int i, int length) {
    int temp = arr[i];
    for (int k = 2 * i + 1; k < length; k = k * 2 + 1) {
      if (k + 1 < length && arr[k + 1] < arr[k]) {
        k++;
      }
      if (arr[k] < temp) {
        arr[i] = arr[k];
        i = k;
      } else {
        break;
      }
    }
    arr[i] = temp;

  }

  private static void swap(int[] arr, int i, int j) {
    int temp = arr[j];
    arr[j] = arr[i];
    arr[i] = temp;
  }

}