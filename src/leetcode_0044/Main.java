package leetcode_0044;

public class Main {


  /**
   * https://leetcodechina.com/problems/length-of-last-word/description/
   * <p>
   * 给定一个字符串， 包含大小写字母、空格 ' '，请返回其最后一个单词的长度。
   * 如果不存在最后一个单词，请返回 0 。
   * 注意事项：一个单词的界定是，由字母组成，但不包含任何的空格。
   *
   * @param s 字符串
   * @return 最后一个单词长度
   * <p>
   * <p>
   * 第一次出错 i >= 0 写成了 i==0
   * 第二次出错  if (findFirstNotEmpty) 写反了
   */
  public int lengthOfLastWord(String s) {
    if (s.length() < 1) {
      return 0;
    }
    int length = 0;
    boolean findFirstNotEmpty = false;
    for (int i = s.length() - 1; i >= 0; i--) {
      if (s.charAt(i) == ' ') {
        if (findFirstNotEmpty) {
          return length;
        }
      } else {
        findFirstNotEmpty = true;
        length = length + 1;
      }
    }
    return length;
  }

}
