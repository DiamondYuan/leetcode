#!/bin/bash

export src=$(printf "src/leetcode_%04d" $1)
export package=$(printf "leetcode_%04d;" $1)
export file=$(printf "src/leetcode_%04d/Main.java" $1)

echo $src

echo $package



mkdir $src

cat >$file <<EOF
package $package

public class Main {
}
EOF